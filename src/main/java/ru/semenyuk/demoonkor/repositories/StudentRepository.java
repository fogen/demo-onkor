package ru.semenyuk.demoonkor.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenyuk.demoonkor.entities.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
