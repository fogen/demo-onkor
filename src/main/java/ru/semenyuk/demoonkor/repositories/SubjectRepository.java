package ru.semenyuk.demoonkor.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenyuk.demoonkor.entities.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {

    Subject findBySubjectName(String subjectName);
}
