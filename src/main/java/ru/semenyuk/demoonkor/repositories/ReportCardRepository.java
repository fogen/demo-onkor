package ru.semenyuk.demoonkor.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenyuk.demoonkor.entities.ReportCard;
import ru.semenyuk.demoonkor.entities.Student;
import ru.semenyuk.demoonkor.entities.Subject;

import java.util.List;

public interface ReportCardRepository extends JpaRepository<ReportCard, Integer> {

    List<ReportCard> findAllByStudent(Student student);

    List<ReportCard> findAllBySubject(Subject subject);
}
