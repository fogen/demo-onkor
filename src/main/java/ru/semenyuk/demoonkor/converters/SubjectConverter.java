package ru.semenyuk.demoonkor.converters;

import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionException;
import ru.semenyuk.demoonkor.entities.Subject;
import ru.semenyuk.demoonkor.repositories.SubjectRepository;

public class SubjectConverter implements Converter<String, Subject> {

    @Autowired
    SubjectRepository subjectRepository;

    @Override
    public Result<Subject> convertToModel(String fieldValue, ValueContext context) {
        try {
            return Result.ok(subjectRepository.findBySubjectName(fieldValue));
        } catch (ConversionException e) {
            return Result.error("Предмет не найден");
        }
    }

    @Override
    public String convertToPresentation(Subject subject, ValueContext valueContext) {
        return subject.toString();
    }


}
