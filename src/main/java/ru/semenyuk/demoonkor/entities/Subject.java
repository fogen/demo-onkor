package ru.semenyuk.demoonkor.entities;

import javax.persistence.Entity;

@Entity
public class Subject extends AbstractEntity {

    private String subjectName;

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @Override
    public String toString() {
        return subjectName;
    }
}
