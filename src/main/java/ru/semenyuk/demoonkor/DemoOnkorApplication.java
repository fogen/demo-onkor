package ru.semenyuk.demoonkor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoOnkorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoOnkorApplication.class, args);
    }

}
