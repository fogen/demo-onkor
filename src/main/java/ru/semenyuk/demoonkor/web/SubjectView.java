package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import ru.semenyuk.demoonkor.entities.Subject;
import ru.semenyuk.demoonkor.repositories.SubjectRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Route(value = "subjects", layout = MainView.class)
@PageTitle("Список предметов")
public class SubjectView extends AppLayout {

    VerticalLayout layout;
    Grid<Subject> grid;
    RouterLink linkCreate;

    @Autowired
    SubjectRepository subjectRepository;

    public SubjectView() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Новый предмет", SubjectEditView.class, 0);
        layout.add(linkCreate);
        layout.add(grid);
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        List<Subject> subjects = subjectRepository.findAll();

        if (!subjects.isEmpty()) {
            grid.addColumn(Subject::getSubjectName).setHeader("Наименование");
            grid.addColumn(new NativeButtonRenderer<Subject>("Редактировать", subject -> {
                UI.getCurrent().navigate(SubjectEditView.class, subject.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<Subject>("Удалить", subject -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить");
                Button cancel = new Button("Отмена");
                dialog.add("Вы уверены что хотите удалить ученика?");
                dialog.add(confirm);
                dialog.add(cancel);
                confirm.addClickListener(clickEvent -> {
                    subjectRepository.delete(subject);
                    dialog.close();
                    Notification notification = new Notification("Ученик удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();
                    grid.setItems(subjectRepository.findAll());
                });
                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });
                dialog.open();
            }));
            grid.setItems(subjects);
        }
    }

}
