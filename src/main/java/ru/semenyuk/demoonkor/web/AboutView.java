package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.Version;

@Route(value = "about", layout = MainView.class)
public class AboutView extends Div {

    VerticalLayout layout;

    public AboutView() {
        layout = new VerticalLayout();
        layout.add(new Label("Это приложннин использует Vaadin version " + Version.getFullVersion() + "."));
        add(layout);
    }
}
