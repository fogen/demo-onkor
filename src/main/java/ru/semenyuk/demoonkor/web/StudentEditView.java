package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.semenyuk.demoonkor.entities.Student;
import ru.semenyuk.demoonkor.repositories.StudentRepository;

import java.util.Optional;

@Route("studentEdit")
@PageTitle("Редактирование ученика")
public class StudentEditView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout studentForm;
    TextField firstName;
    TextField lastName;
    Button saveStudent;

    @Autowired
    StudentRepository studentRepository;

    public StudentEditView() {
        studentForm = new FormLayout();
        firstName = new TextField("Имя");
        lastName = new TextField("Фамилия");
        saveStudent = new Button("Сохранить");
        studentForm.add(firstName, lastName, saveStudent);
        setContent(studentForm);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer studentId) {
        id = studentId;
        fillForm();
    }

    private void fillForm() {
        if (!id.equals(0)) {
            Optional<Student> student = studentRepository.findById(id);
            student.ifPresent(x -> {
                firstName.setValue(x.getFirstName());
                lastName.setValue(x.getLastName());
            });
        }
        saveStudent.addClickListener(clickEvent -> {
            Student student = new Student();
            if (!id.equals(0)) {
                student.setId(id);
            }
            student.setFirstName(firstName.getValue());
            student.setLastName(lastName.getValue());
            studentRepository.save(student);

            Notification notification = new Notification(id.equals(0) ? "Ученик успешно создан" : "Ученик был изменен", 1000);
            notification.setPosition(Notification.Position.MIDDLE);
            notification.addDetachListener(detachEvent -> {
                UI.getCurrent().navigate(StudentView.class);
            });
            studentForm.setEnabled(false);
            notification.open();
        });
    }
}
