package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.semenyuk.demoonkor.entities.Subject;
import ru.semenyuk.demoonkor.repositories.SubjectRepository;

import java.util.Optional;

@Route("subjectEdit")
@PageTitle("Редактирование предмета")
public class SubjectEditView extends AppLayout implements HasUrlParameter<Integer> {

    Integer id;
    FormLayout subjectForm;
    TextField subjectName;
    Button saveSubject;

    @Autowired
    SubjectRepository subjectRepository;

    public SubjectEditView() {
        subjectForm = new FormLayout();
        subjectName = new TextField("Наименование");
        saveSubject = new Button("Сохранить");
        subjectForm.add(subjectName, saveSubject);
        setContent(subjectForm);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Integer subjectId) {
        id = subjectId;
        fillForm();
    }

    private void fillForm() {
        if (!id.equals(0)) {
            Optional<Subject> subject = subjectRepository.findById(id);
            subject.ifPresent(x -> {
                subjectName.setValue(x.getSubjectName());
            });
        }
        saveSubject.addClickListener(clickEvent -> {
            Subject subject = new Subject();
            if (!id.equals(0)) {
                subject.setId(id);
            }
            subject.setSubjectName(subjectName.getValue());
            subjectRepository.save(subject);

            Notification notification = new Notification(id.equals(0) ? "Ученик успешно создан" : "Ученик был изменен", 1000);
            notification.setPosition(Notification.Position.MIDDLE);
            notification.addDetachListener(detachEvent -> {
                UI.getCurrent().navigate(SubjectView.class);
            });
            subjectForm.setEnabled(false);
            notification.open();
        });
    }
}
