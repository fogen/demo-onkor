package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.semenyuk.demoonkor.entities.ReportCard;
import ru.semenyuk.demoonkor.entities.Student;
import ru.semenyuk.demoonkor.entities.Subject;
import ru.semenyuk.demoonkor.repositories.ReportCardRepository;
import ru.semenyuk.demoonkor.repositories.StudentRepository;
import ru.semenyuk.demoonkor.repositories.SubjectRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Route(value = "report", layout = MainView.class)
@PageTitle("Оценки")
public class ReportCardView extends Div {

    private Grid<ReportCard> grid;
    private ComboBox<Student> students = new ComboBox<>();
    private ComboBox<Subject> subjects = new ComboBox<>();
    private TextField score = new TextField();
    private Button cancel = new Button("Отмена");
    private Button save = new Button("Сохранить");
    private Integer editedId = 0;

    private Binder<ReportCard> binder;

    @Autowired
    ReportCardRepository reportCardRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    SubjectRepository subjectRepository;

    public ReportCardView() {
        binder = new Binder<>(ReportCard.class);
        binder.forField(score)
                .withConverter(new StringToIntegerConverter("Оценка должна быть числом"))
                .withValidator(integer -> integer > 0 && integer < 6, "Оценка должна быть в диапозоне от 1 до 5")
                .bind(ReportCard::getScore, ReportCard::setScore);
        binder.forField(students).bind(ReportCard::getStudent, ReportCard::setStudent);
        binder.forField(subjects).bind(ReportCard::getSubject, ReportCard::setSubject);
        grid = new Grid<>(ReportCard.class);
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
        grid.setHeightFull();
        grid.asSingleSelect().addValueChangeListener(event -> populateForm(event.getValue()));
        binder.bindInstanceFields(this);

        SplitLayout splitLayout = new SplitLayout();
        splitLayout.setSizeFull();
        createGridLayout(splitLayout);
        createEditorLayout(splitLayout);
        add(splitLayout);
    }

    @PostConstruct
    public void fillGrid() {

        List<Student> studentList = studentRepository.findAll();
        if (studentList.isEmpty()) {
            UI.getCurrent().navigate("students");
        }

        List<Subject> subjectList = subjectRepository.findAll();
        if (subjectList.isEmpty()) {
            UI.getCurrent().navigate("subjects");
        }

        students.setItems(studentList);
        subjects.setItems(subjectList);
        students.setItemLabelGenerator(Student::toString);
        subjects.setItemLabelGenerator(Subject::getSubjectName);
        cancel.addClickListener(e -> {
            grid.asSingleSelect().clear();
            editedId = 0;
        });
        save.addClickListener(e -> {
            ReportCard reportCard = new ReportCard();
            if (!editedId.equals(0)) {
                reportCard.setId(editedId);
            }
            reportCard.setScore(Integer.valueOf(score.getValue()));
            reportCard.setSubject(subjects.getValue());
            reportCard.setStudent(students.getValue());
            reportCardRepository.save(reportCard);

            Notification notification = new Notification(editedId.equals(0) ? "Ученик успешно создан" : "Ученик был изменен", 1000);
            notification.open();
            grid.setItems(reportCardRepository.findAll());
            grid.asSingleSelect().clear();
            editedId = 0;
        });

        List<ReportCard> reportCards = reportCardRepository.findAll();

        if (!reportCards.isEmpty()) {
            grid.addColumn(ReportCard::getStudent).setHeader("Ученик");
            grid.addColumn(ReportCard::getSubject).setHeader("Предмет");
            grid.addColumn(ReportCard::getScore).setHeader("Оценка");
            grid.addColumn(new NativeButtonRenderer<ReportCard>("Удалить", reportCard -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить");
                Button cancel = new Button("Отмена");
                dialog.add("Вы уверены что хотите удалить ученика?");
                dialog.add(confirm);
                dialog.add(cancel);
                confirm.addClickListener(clickEvent -> {
                    reportCardRepository.delete(reportCard);
                    dialog.close();
                    Notification notification = new Notification("Ученик удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();
                    grid.setItems(reportCardRepository.findAll());
                });
                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });
                dialog.open();
            }));
            grid.setItems(reportCards);
        }
    }

    private void createGridLayout(SplitLayout splitLayout) {
        Div wrapper = new Div();
        wrapper.setId("grid-wrapper");
        wrapper.setWidthFull();
        splitLayout.addToPrimary(wrapper);
        wrapper.add(grid);
    }

    private void createEditorLayout(SplitLayout splitLayout) {
        Div editorLayoutDiv = new Div();
        editorLayoutDiv.setId("editor-layout");

        Div editorDiv = new Div();
        editorDiv.setId("editor");
        editorLayoutDiv.add(editorDiv);

        FormLayout formLayout = new FormLayout();
        addFormItem(editorDiv, formLayout, students, "Ученик");
        addFormItem(editorDiv, formLayout, subjects, "Предмет");
        addFormItem(editorDiv, formLayout, score, "Оценка");
        createButtonLayout(editorLayoutDiv);

        splitLayout.addToSecondary(editorLayoutDiv);
    }

    private void addFormItem(Div wrapper, FormLayout formLayout, AbstractField field, String fieldName) {
        formLayout.addFormItem(field, fieldName);
        wrapper.add(formLayout);
        field.getElement().getClassList().add("full-width");
    }

    private void createButtonLayout(Div editorLayoutDiv) {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setId("button-layout");
        buttonLayout.setWidthFull();
        buttonLayout.setSpacing(true);
        cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonLayout.add(save, cancel);
        editorLayoutDiv.add(buttonLayout);
    }

    private void populateForm(ReportCard value) {
        binder.readBean(value);
        editedId = value.getId();
    }
}
