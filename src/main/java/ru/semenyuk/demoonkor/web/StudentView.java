package ru.semenyuk.demoonkor.web;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import ru.semenyuk.demoonkor.entities.Student;
import ru.semenyuk.demoonkor.repositories.StudentRepository;

import javax.annotation.PostConstruct;
import java.util.List;

@Route(value = "students", layout = MainView.class)
@PageTitle("Список учеников")
public class StudentView extends AppLayout {

    VerticalLayout layout;
    Grid<Student> grid;
    RouterLink linkCreate;

    @Autowired
    StudentRepository studentRepository;

    public StudentView() {
        layout = new VerticalLayout();
        grid = new Grid<>();
        linkCreate = new RouterLink("Новый ученик", StudentEditView.class, 0);
        layout.add(linkCreate);
        layout.add(grid);
        setContent(layout);
    }

    @PostConstruct
    public void fillGrid() {
        List<Student> students = studentRepository.findAll();

        if (!students.isEmpty()) {
            grid.addColumn(Student::getFirstName).setHeader("Имя");
            grid.addColumn(Student::getLastName).setHeader("Фамилия");
            grid.addColumn(new NativeButtonRenderer<Student>("Редактировать", student -> {
                UI.getCurrent().navigate(StudentEditView.class, student.getId());
            }));
            grid.addColumn(new NativeButtonRenderer<Student>("Удалить", student -> {
                Dialog dialog = new Dialog();
                Button confirm = new Button("Удалить");
                Button cancel = new Button("Отмена");
                dialog.add("Вы уверены что хотите удалить ученика?");
                dialog.add(confirm);
                dialog.add(cancel);
                confirm.addClickListener(clickEvent -> {
                    studentRepository.delete(student);
                    dialog.close();
                    Notification notification = new Notification("Ученик удален", 1000);
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.open();
                    grid.setItems(studentRepository.findAll());
                });
                cancel.addClickListener(clickEvent -> {
                    dialog.close();
                });
                dialog.open();
            }));
            grid.setItems(students);
        }
    }
}
